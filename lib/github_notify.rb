module GithubNotify
  require "net/http"
  require "uri"
  require "libnotify"
  require 'json'
  require 'github_notify/config'

  $notifications = []

  def self.poll_github(username, token)
    uri = URI.parse("https://api.github.com/notifications")

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)
    request.basic_auth(username, token)
    response = http.request(request)

    return JSON.parse(response.body)
  end

  def self.show_notification(notification)
    n = Libnotify.new do |notify|
      body = "\"#{notification['subject']['title']}\" @ repo: #{notification['repository']['name']}"
      notify.summary    = notification["subject"]["type"]
      notify.body       = body
      notify.timeout    = 1           # 1.5 (s), 1000 (ms), "2", nil, false
      notify.urgency    = :critical     # :low, :normal, :critical
      notify.append     = false       # default true - append onto existing notification
      notify.transient  = true        # default false - keep the notifications around after display
    end
    n.show!
  end

  def self.run(config)
    extend Config

    config = load_config(config)
    username = config["account"]["username"]
    token = config["account"]["access_token"]
    sleep_time = config["sleep"]

    count = 0
    loop do
      notifications = poll_github(username, token)

      if notifications.count > 0
        notifications.each do |notification|
          unless $notifications.include?(notification)
            show_notification(notification)
            $notifications << notification
          end
        end

        if count > 10
          $notifications.each {|notification| show_notification(notification);}
          $notifications = []
          count = 0
        end

        sleep sleep_time
        count += 1
      end
    end
  end
end
