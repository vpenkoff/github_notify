require 'yaml'

module Config

  def load_config(config_file)
    if config_exists?(config_file)
      parsed_config = YAML::load_file(config_file)
    else
      abort("No config file could be found at #{$config}")
    end
    parsed_config
  end

  def config_exists?(config_file)
    File.exist?(config_file)
  end
end
