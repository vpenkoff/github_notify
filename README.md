# GithubNotify

GithubNotify is a simple notifier for notifications in GitHub. It stays in the background and checks if there are
any notifications.

## Prerequesites
Before use, one must obtain a [personal access token] (https://github.com/settings/tokens) for his GitHub account.
The scope needed is "notifications".

After creating an access token, feed the config file with it.

## Installation
1. Clone the repo
2. Execute ```bundle install```

## Usage
* Execute ```nohup ./run &```


## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/vpenkoff/github_notify.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

